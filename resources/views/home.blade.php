@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                    <section>
                      @if (isset ($questionnaires))

                        <ul>
                          @foreach ($questionnaires as $questionnaire)
                            <li>{{ $questionnaire->Name}}</li>
                          @endforeach
                        </ul>
                      @else
                        <p> no questionnaires added yet </p>
                      @endif
                    </section>


                    {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
                        <div class="row">
                            {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
