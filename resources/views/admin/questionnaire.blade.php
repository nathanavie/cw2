<<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Questionnaire</title>
</head>
<body>
  <h1>Questionnaire</h1>

  <section>
    @if (isset ($questionnaires))

      <ul>
        @foreach ($questionnaires as $questionnaire)
          <li>{{ $questionnaire->Name}}</li>
        @endforeach
      </ul>
    @else
      <p> no questionnaires added yet </p>
    @endif
  </section>


  <div class="row">
  {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
          {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
      </div>
  {{ Form::close() }}
</body>
</html>
