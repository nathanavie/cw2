<?php


/*
This is the creation of the question table where the questions will be stored
the table has a foreign key of the Questionnaire ID field, a QuestionID field, Name field, and 5 separate answer fields.
*/
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Question extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('question', function (Blueprint $table) {
        $table->integer('questionnaire');
        $table->increments('QuestionID');
        $table->string('QuestionName');
        $table->string('Answer1');
        $table->string('Answer2');
        $table->string('Answer3');
        $table->string('Answer4');
        $table->string('Answer5');
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('question');
    }
}
