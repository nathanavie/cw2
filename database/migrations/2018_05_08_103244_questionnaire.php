<?php

/*
This is the creation of the questionnaire table where the questionnaires will be stored
The table has an ID field, Name field, and Description field
*/
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Questionnaire extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('questionnaire', function (Blueprint $table) {
        $table->increments('questionnaireID');
        $table->string('Name')->unique();
        $table->text('Description');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questionnaire');
    }
}
