<?php


/*
This is the creation of the responses table where the information that the user provides will be stored
The table has a foreign key of the Question ID field, a response ID field, and the response field.
*/
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Responses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('responses', function (Blueprint $table) {
          $table->integer('questionID');
          $table->increments('ResponseID');
          $table->string('Response');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('responses');
    }
}
